====================
 xoeuf's Odoo tools
====================

xoeuf provides utilities to ease working with Odoo's framework.

Documentation is a work in progress, but it can be found in
https://merchise-autrement.gitlab.io/xoeuf/.

See DEVELOPING to know how to setup the developer's environment.
