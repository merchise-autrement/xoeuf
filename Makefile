CARGO_HOME ?= $(HOME)/.cargo
PATH := $(HOME)/.rye/shims:$(HOME)/.local/bin:$(CARGO_HOME)/bin:$(PATH)

SHELL := /bin/bash

PYTHON_FILES := $(shell find src/ -type f -name '*.py')

UV := uv
UV_RUN := $(UV) run
RUN ?= $(UV_RUN) --no-sync

ODOO_VERSION ?= 12.0
PYTHON_VERSION ?= 3.8.17

REQUIRED_UV_VERSION ?= 0.5.31
bootstrap:
	@INSTALLED_UV_VERSION=$$(uv --version 2>/dev/null | awk '{print $$2}' || echo "0.0.0"); \
    UV_VERSION=$$(printf '%s\n' "$(REQUIRED_UV_VERSION)" "$$INSTALLED_UV_VERSION" | sort -V | head -n1); \
	if [ "$$UV_VERSION" != "$(REQUIRED_UV_VERSION)" ]; then \
		curl -LsSf https://astral.sh/uv/install.sh | sh; \
	fi
	echo $(PYTHON_VERSION) > .python-version
.PHONY: bootstrap

sync install: bootstrap src/odoo-$(ODOO_VERSION)
	@$(UV) sync --frozen
ifndef lite
	@$(UV) pip install --no-deps -e src/odoo-$(ODOO_VERSION)
	@$(UV) pip install -r src/odoo-$(ODOO_VERSION)/requirements.txt
endif
.PHONY: install sync

lock: bootstrap
ifdef update_all
	@$(UV) lock -U
else
	@$(UV) lock
endif
ifndef lite
	@$(UV) pip install -r src/odoo-$(ODOO_VERSION)/requirements.txt
	@$(UV) pip install --no-deps -e src/odoo-$(ODOO_VERSION)
endif
.PHONY: lock

update: bootstrap
	@$(MAKE) lock update_all=1
.PHONY: update

format: install
	@$(RUN) ruff check --fix src/
	@$(RUN) ruff format src/
	@$(RUN) isort src/
.PHONY: format-python

lint: install
	@$(RUN) ruff check src/
	@$(RUN) ruff format --check src/
	@$(RUN) isort --check src/
.PHONY: lint

docs:
	@$(MAKE) SPHINXBUILD="$(RUN) sphinx-build" -C docs html
.PHONY: docs

CADDY_SERVER_PORT ?= 9999
caddy: docs
	@docker run --rm -p $(CADDY_SERVER_PORT):$(CADDY_SERVER_PORT) \
         -v $(PWD)/docs/build/html:/var/www -it caddy \
         caddy file-server --browse --listen :$(CADDY_SERVER_PORT) --root /var/www
.PHONY: caddy


# You can tweak these defaults in '.envrc' or by passing them to 'make'.
POSTGRES_USER ?= $(USER)
POSTGRES_PASSWORD ?= $(USER)
POSTGRES_HOST ?= pg
ODOO_MIRROR ?= https://gitlab.merchise.org/merchise/odoo.git
DOCKER_NETWORK_ARG ?= --network=host

test_addons ?=
test_tags ?=
test_extra_args ?=
RUN_UV_ARG ?= --run-uv

test:
	if [ -n "$(test_addons)" ]; then \
		addons="$(test_addons)"; \
    else \
		addons=$$(ls src/tests/ | grep '^test_' | xargs | tr " " ","); \
	fi; \
	if [ -n "$(test_tags)" ]; then \
		tags="--test-tags $(test_addons)"; \
    else \
		tags=""; \
	fi; \
	./runtests-odoo.sh $(RUN_UV_ARG) -i $$addons $$tags $(test_extra_args)
.PHONY: test

shell:
	$(RUN) ipython
.PHONY: shell

docker/build:
	docker build -t xoeuf \
	    --build-arg PYTHON_VERSION=$(PYTHON_VERSION) \
	    --build-arg ODOO_VERSION=$(ODOO_VERSION) \
	    --build-arg ODOO_MIRROR=$(ODOO_MIRROR) \
        .
.PHONY: docker/build

docker/test: docker/build
	docker run --rm -it xoeuf /src/xoeuf/runtests-odoo.sh \
        -i $(ls src/tests/ | grep '^test_' | xargs | tr " " ",") \
		$(DOCKER_NETWORK_ARG) \
        --db_host=$(POSTGRES_HOST) \
        --db_user=$(POSTGRES_USER) \
        --db_password=$(POSTGRES_PASSWORD)
.PHONY: docker/test docker/build


ODOO_MIRROR ?= https://gitlab.merchise.org/merchise/odoo.git

src/odoo-$(ODOO_VERSION):
ifndef lite
	@if [ ! -d src/odoo-$(ODOO_VERSION) ]; then \
		git clone --depth 1 -b merchise-develop-$(ODOO_VERSION) \
           $(ODOO_MIRROR) \
           src/odoo-$(ODOO_VERSION); \
    else \
        cd src/odoo-$(ODOO_VERSION); git pull; \
		cd ../../; \
    fi; \
	rm -rf src/odoo-$(ODOO_VERSION)/pyproject.toml; \
	rm -rf src/odoo; \
	cd src; ln -s odoo-$(ODOO_VERSION) odoo; \
	cd odoo; ln -s ../../extra/pyproject-odoo-$(ODOO_VERSION).toml pyproject.toml
endif
.PHONY: src/odoo-$(ODOO_VERSION)
