#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# Copyright (c) Merchise Autrement [~º/~] and Contributors
# All rights reserved.
#
# This is free software; you can do what the LICENCE file allows you to.
#

{
    "name": "test_uuid_field",
    "author": "Merchise Autrement [~º/~] and Contributors",
    "description": "Test for the fields.UUID",
    "depends": ["base"],
    "installable": True,
    "auto_install": False,
}
