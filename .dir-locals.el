;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((python-mode . ((projectile-project-compilation-cmd . "make test")
                  (fill-column . 100))))
